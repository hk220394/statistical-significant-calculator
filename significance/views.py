# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.http import HttpResponse
from django.shortcuts import render
import math
import scipy.stats
from scipy import stats
from scipy.stats import norm


# Create your views here.
def index(request):
	return render(request, 'significance/significance.html')


def significant(request):
	visitors_c = request.GET.get('visitors_c')	
	visitors_v = request.GET.get('visitors_v')
	conversions_c = request.GET.get('conversions_c')
	conversions_v = request.GET.get('conversions_v')

	control_p = (float(conversions_c) / float(visitors_c))
	
	variation_p = (float(conversions_v) / float(visitors_v))

	control_se = math.sqrt((control_p*(1-control_p)/float(visitors_c)))

	variation_se = math.sqrt((variation_p*(1-variation_p)/float(visitors_v)))

	
	# CCRL = Control Conversion Rate Limit
	# VCRL = Variation Conversion Rate Limit


	# 90% CCRL
	if ((control_p-1.65*control_se)<0):
		one_ccrl_from = (control_p-1.65*control_se)
		
	else:
		one_ccrl_from = (control_p-1.65*control_se)
		
	if ((control_p+1.65*control_se)>1):
		one_ccrl_to = control_p+1.65*control_se
		
	else:
		one_ccrl_to = (control_p+1.65*control_se)


	# 90% VCRL
	if ((variation_p-1.65*variation_se)<0):
		one_vcrl_from = (variation_p-1.65*variation_se)
		
	else:
		one_vcrl_from = (variation_p-1.65*variation_se)
		
	if ((variation_p+1.65*variation_se)>1):
		one_vcrl_to = variation_p+1.65*variation_se
		
	else:
		one_vcrl_to = (variation_p+1.65*variation_se)


	


	# 95% CCRL
	if ((control_p-1.96*control_se)<0):
		two_ccrl_from = (control_p-1.96*control_se)
		
	else:
		two_ccrl_from = (control_p-1.96*control_se)
		
	if ((control_p+1.96*control_se)>1):
		two_ccrl_to = control_p+1.96*control_se
		
	else:
		two_ccrl_to = (control_p+1.96*control_se)

	# 95% VCRL

	if ((variation_p-1.96*variation_se)<0):
		two_vcrl_from = (variation_p-1.96*variation_se)
		
	else:
		two_vcrl_from = (variation_p-1.96*variation_se)
		
	if ((variation_p+1.96*variation_se)>1):
		two_vcrl_to = variation_p+1.96*variation_se
		
	else:
		two_vcrl_to = (variation_p+1.96*variation_se)
		
	


	# 99% CCRL 

	if ((control_p-2.57*control_se)<0):
		three_ccrl_from = (control_p-2.57*control_se)
		
	else:
		three_ccrl_from = (control_p-2.57*control_se)
		
	if ((control_p+2.57*control_se)>1):
		three_ccrl_to = control_p+2.57*control_se
		
	else:
		three_ccrl_to = (control_p+2.57*control_se)
		
	# 99% VCRL 

	if ((variation_p-2.57*variation_se)<0):
		three_vcrl_from = (variation_p-2.57*variation_se)
		
	else:
		three_vcrl_from = (variation_p-2.57*variation_se)
		
	if ((variation_p+2.57*variation_se)>1):
		three_vcrl_to = variation_p+2.57*variation_se
		
	else:
		three_vcrl_to = (variation_p+2.57*variation_se)
	


#Z-score
	pcontrol_se = math.pow(control_se,2)
	pvariation_se = math.pow(variation_se,2)

	standard = math.sqrt(pcontrol_se + pvariation_se)

	print standard
#P-Value
	if (standard == 0.0):
		p_value = "NaN"
	else:	
		Z_Score = (control_p-variation_p) / standard
		p_value = (1 - (norm.cdf(Z_Score)))
		p_value = float("%.3f" % p_value)	
	


	print p_value	
	Significance = 0
# Significance 
	
	# 99% Confidence
	if ((p_value<0.01) or (p_value>0.99)):
		Significance = "Yes"
		
	# 95% Confidence	
	elif((p_value<0.05) or (p_value>0.95)):	
		Significance = "Yes"
		
	# 90% Confidence	
	elif ((p_value<0.1) or (p_value>0.9)):
		Significance = "Yes"
		
	else:
		Significance = "No"
		

	context = {'Significance' : Significance, 'p_value' : p_value }
	return render(request, 'significance/significance.html', context)	

		
		
