from django.conf.urls import url
from django.conf import settings
from django.conf.urls.static import static
from significance import views

app_name = 'significance'
urlpatterns = [
    url(r'^significance/$', views.index),
    url(r'^ajax/significant/$', views.significant),
]

if settings.DEBUG:
    urlpatterns = urlpatterns + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)