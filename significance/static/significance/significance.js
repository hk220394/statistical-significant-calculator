 $(document).ready(function(){
   
    $(".btn-primary").click(function(){
    	
    	var visitors_c = document.getElementById("visitors_c").value; 
    	var visitors_v = document.getElementById("visitors_v").value;
    	var conversions_c = document.getElementById("conversion_c").value;
    	var conversions_v= document.getElementById("conversion_v").value;

        		console.log(visitors_c,visitors_v,conversions_c,conversions_v);
                $.ajax({
                url: '/ajax/significant/',
                data: {
                	csrfmiddlewaretoken: document.getElementsByName('csrfmiddlewaretoken')[0].value,
                	'visitors_c':visitors_c,
                	'visitors_v':visitors_v,
                	'conversions_c': conversions_c,
                	'conversions_v':conversions_v
                },
                type: 'GET',
                dataType: "html",
                success: function (response) {
                console.log(response);
                $('#p_value').replaceWith($(response).find("#p_value"));
                $('#significance').replaceWith($(response).find("#significance"));
                $("#result").slideDown();
        },
    });   
    });
});
